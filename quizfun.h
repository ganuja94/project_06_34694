#ifndef QUIZFUN_H 
#define QUIZFUN_H
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <conio.h>


using namespace std;

class Question {
	static int Score;
private:
	string text_Question;
	string Answer_1;
	string Answer_2;
	string Answer_3;
	string Answer_4;
	string correct_Answer;
public:
	Question();
	Question(string text_Question, string Answer_1, string Answer_2, string Answer_3, string Answer_4, string correct_Answer);

	void askQuestion();
	string get_text_Question();
	string get_Answer_1();
	string get_Answer_2();
	string get_Answer_3();
	string get_Answer_4();
	string get_correct_Answer();
	int get_Score();
	void addScore();

};

Question setQuestion(Question tab[]);
void menuControler();
void startQuiz();
void endQuiz(Question tab[], string ans_tab[]);

#endif
