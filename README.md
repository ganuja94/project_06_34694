# README #

### Opis funkcjonalności

oid menuControler() - celem tej funkcji jest wyświetlenie prostego menu

void startQuiz() - jej celem jest sam quiz, tutaj odpowiada się na pytania quizu

Question setQuestion( Question tab[] ) - przyjmuje ona jako argument tablice klasy Question,
ma za zadanie przetowrzyć plik csv na pytania (Obiekty klasy Question)

void endQuiz() - sprawdza czy zdalismy quiz, generuje raport do pliku tekstowego

Funkcje klasy Question:

string get_text_Question()
string get_Answer_1()
string get_Answer_2()
string get_Answer_3()
string get_Answer_4()
string get_correct_Answer()
int get_Score() 

-- Gettery klasy Question


void askQuestion() - Wyświetla pytanie

void addScore() - Zwiększa licznik punktów 

Próg zaliczenia jest zwiazany ze stałą numQuestion

Klasa:

Klasa ma za zadanie utworzyć pytanie (jeden obiekt klasy questions odpowiada jednemu pytaniu)



### Informacje techniczne

Aplikacja napisana w oparciu o język C++ przy użyciu Visual Studio.

### Działanie quizu

Po uruchomieniu quizu uruchamia sie menu

Wybieramy opcje rozpoczecia gry albo wyjscia z programu

Poprawna odpowiedz wpisujemy z klawiatury a/b/c/d

Po zakonczonym quizie generuje sie raport a my mozemy wystartowac od nowa

Raport znajdziemy pod nazwa raport w folderze głównym

### Podział pracy

Robert	34694	
Struktura plikow				  
Funkcja wywoływania quizu		
Funkcja podsumowująca z raportem  

Maksymilian	 34315			
Klasa pytania 
Funkcja do odczytwania danych csv
Funkcja do wyswietlania pytania	




