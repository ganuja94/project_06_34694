#include "quizfun.h"

int Question::Score = 0;
const int num_Question = 10;

Question::Question()
{
	this->text_Question;
	this->Answer_1;
	this->Answer_2;
	this->Answer_3;
	this->Answer_4;
	this->correct_Answer;
}

Question::Question(string t_Question, string A_1, string A_2, string A_3, string A_4, string c_Answer)
{
	this->text_Question = t_Question;
	this->Answer_1 = A_1;
	this->Answer_2 = A_2;
	this->Answer_3 = A_3;
	this->Answer_4 = A_4;
	this->correct_Answer = c_Answer;
}


string Question::get_text_Question()
{
	return string(this->text_Question);
}

string Question::get_Answer_1()
{
	return string(this->Answer_1);
}

string Question::get_Answer_2()
{
	return string(this->Answer_2);
}

string Question::get_Answer_3()
{
	return string(this->Answer_3);
}

string Question::get_Answer_4()
{
	return string(this->Answer_4);
}

string Question::get_correct_Answer()
{
	return string(this->correct_Answer);
}

int Question::get_Score()
{
	return int(this->Score);
}

void Question::addScore()
{
	Score = Score + 1;
}

void Question::askQuestion()
{
	cout << endl;
	cout << "\t\t\t\t\t\t\t\t Punktacja = " << Score << endl;
	cout << this->text_Question << endl;
	cout << "a:" << this->Answer_1 << endl;
	cout << "b:" << this->Answer_2 << endl;
	cout << "c:" << this->Answer_3 << endl;
	cout << "d:" << this->Answer_4 << endl;
	cout << "Jaka jest twoja odpowiedz:-";
}

void menuControler()
{
	char options;

	do {
		system("cls");
		cout << "||            ||  ||=====   ||        ====      ====     ||\\  //||  ||======" << endl;
		cout << " ||          ||   ||        ||      ==        ==    ==   || \\// ||  ||      " << endl;
		cout << "  ||   ||   ||    ||====    ||      ==       ==      ==  ||      ||  ||====  " << endl;
		cout << "   || |||| ||     ||        ||      ==        ==    ==   ||      ||  ||      " << endl;
		cout << "    ||    ||      ||======  ||====    ====     =====     ||      ||  ||======" << endl;
		cout << "1. Zagraj w Quiz" << endl;
		cout << "2. Wyjscie" << endl;
		cout << "Wybierz swoja opcje (1/2):" << endl;
		cin >> options;

		switch (options) {
		case '1':
			startQuiz();
			break;
		case '2':
			break;
		default:
			cout << "Podano zly klawisz";
		}
	} while (options != '2');
}

void startQuiz()
{
	Question tab[num_Question];
	Question* ptr;
	ptr = tab;
	string ans_tab[num_Question];
	string guess;
	setQuestion(ptr);
	system("cls");
	for (int i = 0; i < num_Question; i++) {
		cout << "         QUIZ GAME" << endl;
		tab[i].askQuestion();
		cin >> guess;
		system("cls");
		if (guess == tab[i].get_correct_Answer()) {
			cout << "Poprawna odpowiedz" << endl;
			cout << "Kliknij by przejsc dalej" << endl;
			_getch();
			tab->addScore();
			system("cls");
		}
		else {
			cout << "Nie udalo sie" << endl;
			cout << "Kliknij by przejsc dalej" << endl;
			_getch();
			system("cls");
		}
		ans_tab[i] = guess;
	}
	endQuiz(tab, ans_tab);
}

Question setQuestion(Question tab[])
{
	int i = 0;
	int test = 0;
	ifstream q_file;
	string line = "";
	q_file.open("Pytania/dane.csv");
	while (getline(q_file, line))
	{
		vector<string> result;
		stringstream s_stream(line);
		for (int j = 0; j < 6; j++)
		{
			string word;
			getline(s_stream, word, ',');
			result.push_back(word);
		}
		if (i < num_Question) {
			string t_Q = result.at(0);
			string A_1 = result.at(1);
			string A_2 = result.at(2);
			string A_3 = result.at(3);
			string A_4 = result.at(4);
			string c_A = result.at(5);
			tab[i] = Question(t_Q, A_1, A_2, A_3, A_4, c_A);
		}
		i++;
		line = "";
	}

	return *tab;
}

void endQuiz(Question tab[], string ans_tab[])
{
	int prog_zdania = ceil(num_Question * 0.8);
	ofstream myFile;
	myFile.open("Raport/raport.txt");
	if (tab->get_Score() > prog_zdania) {
		cout << "Udalo ci sie zdac quiz na:" << tab->get_Score() << endl << "Prog zdania:" << prog_zdania << endl;
		myFile << "Uda�o ci si� zda� quiz na: " << tab->get_Score() << endl << "Pr�g zdania: " << prog_zdania << endl << endl;
	}
	else {
		cout << "Przykro mi, ale nie udalo ci sie zdac quizu. Twoje punkty: " << tab->get_Score() << endl << "Prog zdania: " << prog_zdania << endl;
		myFile << "Przykro mi, ale nie uda�o ci si� zda� quizu. Twoje punkty : " << tab->get_Score() << endl << "Pr�g zdania: " << prog_zdania << endl << endl;
	}

	for (int i = 0; i < num_Question; i++) {
		myFile << "Pytanie: " << tab[i].get_text_Question() << endl;
		myFile << "Odpowiedz 1: " << tab[i].get_Answer_1() << endl;
		myFile << "Odpowiedz 2: " << tab[i].get_Answer_2() << endl;
		myFile << "Odpowiedz 3: " << tab[i].get_Answer_3() << endl;
		myFile << "Odpowiedz 4 :" << tab[i].get_Answer_4() << endl;
		myFile << "Odpowiedz prawid�owa: " << tab[i].get_correct_Answer() << endl;
		myFile << "Udzielona odpowiedz: " << ans_tab[i] << endl << endl;
	}

	cout << endl << "Kliknij przycisk by powrocic do menu";
	_getch();
}